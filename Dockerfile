FROM python:3.8
WORKDIR /usr/src
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/v1.18.6/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY manager .
CMD ["python","app.py"]
