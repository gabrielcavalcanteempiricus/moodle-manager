import enum

from backend.structs import MoodleConfig


def create_moodle(repository, moodle_config, enterprise="default"):
    return repository.create(moodle_config, enterprise)


def get_moodle(repository, name, enterprise="default") -> MoodleConfig:
    return repository.get_by_name(name, enterprise)


def list_moodles(repository, enterprise) -> MoodleConfig:
    return repository.get_by_enterprise(enterprise)


def delete_moodle(repository, name, enterprise) -> MoodleConfig:
    return repository.delete_by_name(name, enterprise)


class MoodleEventType(enum.Enum):
    CREATE = "create-moodle"
    DELETE = "delete-moodle"


def publish_moodle_event(repository, event_type: MoodleEventType, moodle_config):
    yield repository.publish_message(event_type.value, moodle_config)


def publish_create_moodle_event(repository, moodle_config):
    yield repository.publish_message("create-moodle", moodle_config)

