from functools import partial
from unittest import TestCase

from backend.gateways import create_moodle, publish_create_moodle_event, MoodleEventType, delete_moodle, \
    publish_moodle_event
from backend.structs import MoodleConfig
from backend.tests.fake_repositories import KubernetesFakeRepository
from backend.usecases import create_moodle_use_case, list_moodles_by_enteprise_use_case, delete_moodle_use_case


class StanSpyRepository:
    channel = None
    payload = None

    def publish_message(self, channel, payload):
        self.channel = channel
        self.payload = payload

        return ""


class MoodleUseCaseTests(TestCase):

    def setUp(self) -> None:
        self.kubernetes_repository = KubernetesFakeRepository()
        self.stan_repository = StanSpyRepository()

    def test_list_moodle_by_enterprise_when_enterprise_exists_and_has_moodles(self):
        moodle1 = MoodleConfig(name="moodle1")
        moodle2 = MoodleConfig(name="moodle2")

        result = list_moodles_by_enteprise_use_case(kubernetes_repository=None,
                                                    list_moodle_by_enterprise_gateway=lambda repo,
                                                                                             enterprise: [moodle1,
                                                                                                          moodle2],
                                                    enterprise=None
                                                    )

        self.assertEqual(2, len(result))

    def test_list_moodle_by_enterprise_yields_false_when_enterprise_doesnt_exists(self):
        result = list_moodles_by_enteprise_use_case(kubernetes_repository=None,
                                                    list_moodle_by_enterprise_gateway=lambda repo, enterprise: False,
                                                    enterprise="some-enterprise")

        self.assertFalse(result)

    def test_create_moodle_and_publish_event_succeeds_when_all_information_are_valid(self):
        moodle_config = MoodleConfig(name='moodle1')

        create_moodle_use_case(self.kubernetes_repository,
                               self.stan_repository,
                               create_moodle,
                               publish_create_moodle_event,
                               moodle_config)

        self.assertEqual("create-moodle", self.stan_repository.channel)
        self.assertEqual(moodle_config, self.stan_repository.payload)

    def test_dont_publish_event_when_create_moodle_fails(self):
        create_moodle_use_case(kubernetes_repository=None,
                               stan_repository=None,
                               create_moodle_gateway=lambda repo, config: False,
                               publish_create_moodle_event_gateway=None,
                               moodle_config=None)

        self.assertEqual(None, self.stan_repository.channel)
        self.assertEqual(None, self.stan_repository.payload)

    def test_delete_moodle_when_enterprise_and_moodle_exists(self):
        moodle_config = MoodleConfig(name="moodle1")
        delete_moodle_gateway = partial(delete_moodle, enterprise="default")

        delete_moodle_use_case(self.kubernetes_repository,
                               self.stan_repository,
                               delete_moodle_gateway,
                               publish_moodle_event,
                               moodle_config)

        self.assertEqual(MoodleEventType.DELETE.value, self.stan_repository.channel)

    def test_delete_moodle_yields_false_when_moodle_doesnt_exists(self):
        kubernetes_repository = KubernetesFakeRepository(expected_result=False)
        delete_moodle_gateway = partial(delete_moodle, enterprise="default")
        moodle_config = MoodleConfig(name="WTF")

        delete_moodle_use_case(kubernetes_repository,
                               self.stan_repository,
                               delete_moodle_gateway,
                               publish_moodle_event,
                               moodle_config)

        self.assertIsNone(self.stan_repository.channel)
