import unittest

from backend.gateways import create_moodle, get_moodle, delete_moodle
from backend.structs import MoodleConfig
from backend.tests.fake_repositories import KubernetesFakeRepository


class MoodleConfigTests(unittest.TestCase):
    def setUp(self) -> None:
        self.repository = KubernetesFakeRepository()
        self.moodle_config = MoodleConfig(name="FakeName", version="3.9",
                                          virtual_host="https://fakename.virtualhost.com")

    def test_get_specific_moodle_when_enterprise_and_moodle_exists(self):
        result = get_moodle(self.repository, name="moodle1", enterprise="enterprise1")

        self.assertTrue(result)
        self.assertEqual(self.moodle_config, result)

    def test_if_can_create_a_new_moodle(self):
        result = create_moodle(self.repository, self.moodle_config, enterprise="default")

        self.assertTrue(result)
        instance = get_moodle(self.repository, name="moodle1")
        self.assertIsInstance(instance, MoodleConfig)
        self.assertEqual("FakeName", instance.name)
        self.assertEqual("3.9", instance.version)
        self.assertEqual("https://fakename.virtualhost.com", instance.virtual_host)

    def test_if_can_delete_an_existing_moodle(self):
        result = delete_moodle(self.repository, enterprise="default",
                               name="moodle1"
                               )

        self.assertTrue(result)

    def test_return_false_when_delete_a_moodle_that_doesnt_exists(self):
        result = delete_moodle(KubernetesFakeRepository(expected_result=False),
                               enterprise="default",
                               name="moodle1"
                               )

        self.assertFalse(result)
