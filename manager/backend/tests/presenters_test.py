import json
import unittest

from backend.presenters import present_moodle_config_list_as_json_serializable
from backend.structs import MoodleConfig


class MoodleConfigAPIPresenterTests(unittest.TestCase):
    def test_present_all_moodleconfig_objects_as_json_serializable(self):
        moodle1 = MoodleConfig(name="FakeName", version="3.9", virtual_host="https://fakename.virtualhost.com")
        moodle2 = MoodleConfig(name="FakeName2", version="3.9", virtual_host="https://fakename2.virtualhost.com")

        result = present_moodle_config_list_as_json_serializable([moodle1, moodle2])

        self.assertTrue(result)
        self.assertEqual(2, len(json.loads(result)))
        self.assertTrue(json.dumps(result))
