from backend.repositories import ConfigRepository
from backend.structs import MoodleConfig


class KubernetesFakeRepository(ConfigRepository):
    def __init__(self, expected_result=True):
        self.expected_result = expected_result

    def get_by_name(self, name: str, namespace) -> MoodleConfig:
        return MoodleConfig(
            name="FakeName",
            virtual_host="https://fakename.virtualhost.com",
            version="3.9"
        )

    def create(self, moodle_config, namespace) -> bool:
        return True

    def delete_by_name(self, name: str, namespace) -> bool:
        return self.expected_result
