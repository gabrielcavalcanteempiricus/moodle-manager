import os
import unittest
from unittest.mock import Mock

import kubernetes
from kubeconfig import kubectl
from mockextras import when, Any

from backend.repositories import KubernetesPersistenceRepository, StanRepository
from backend.structs import MoodleConfig


@unittest.skipUnless(os.getenv('ENVIRONMENT', 'local').startswith('local'), 'only with docker-compose locally.')
class NatsStreamingRepositoryTests(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.repository = StanRepository()
        await self.repository.connect()

    async def asyncTearDown(self):
        await self.repository.disconnect()

    async def test_can_publish_message_when_payload_is_encodable(self):
        message_ack = await self.repository.publish_message("some-channel", "some-payload")

        self.assertTrue(message_ack)
        self.assertFalse(message_ack.error)


@unittest.skipUnless(os.getenv('ENVIRONMENT', 'local').startswith('local'), 'only with docker-compose locally.')
class KubernetesRepositoryTests(unittest.TestCase):
    def setUp(self) -> None:
        self.repository = KubernetesPersistenceRepository()
        kubectl.run(subcmd_args=["apply", "-f", "kubernetes/crds/moodle.yaml"])

    def test_can_verify_connection_to_kubernetes(self):
        version = self.repository.get_version()

        self.assertTrue(version)

    def test_get_by_name_returns_false_when_object_doesnt_exists(self):
        self.repository.custom_objects_api.get_namespaced_custom_object = Mock()
        exception = kubernetes.client.rest.ApiException(status=404)
        when(self.repository.custom_objects_api.get_namespaced_custom_object).called_with(group=Any(),
                                                                                          version=Any(),
                                                                                          namespace=Any(),
                                                                                          plural=Any(),
                                                                                          name=Any()).then(exception)

        result = self.repository.get_by_name("FakeName")

        self.assertEqual(False, result)

    def test_get_by_name_raise_exception_when_unknown_exception_occurs(self):
        self.repository.custom_objects_api.get_namespaced_custom_object = Mock()
        exception = kubernetes.client.rest.ApiException(status=500)
        when(self.repository.custom_objects_api.get_namespaced_custom_object).called_with(group=Any(),
                                                                                          version=Any(),
                                                                                          namespace=Any(),
                                                                                          plural=Any(),
                                                                                          name=Any()).then(exception)
        with self.assertRaises(Exception):
            self.repository.get_by_name("FakeName")

    def test_get_by_enterprise_when_all_data_are_valid(self):
        moodle1 = MoodleConfig(name="moodle1")
        moodle2 = MoodleConfig(name="moodle2")
        self.repository.create(moodle1, namespace="default")
        self.repository.create(moodle2, namespace="default")

        result = self.repository.get_by_enterprise("default")

        self.assertEqual(2, len(result))

        self.repository.delete_by_name("moodle1")
        self.repository.delete_by_name("moodle2")

    def test_create_returns_false_when_object_already_exists(self):
        self.repository.custom_objects_api.create_namespaced_custom_object = Mock()
        exception = kubernetes.client.rest.ApiException(status=409)
        moodle_config = MoodleConfig(name="FakeName")
        when(self.repository.custom_objects_api.create_namespaced_custom_object).called_with(group=Any(),
                                                                                             version=Any(),
                                                                                             namespace=Any(),
                                                                                             plural=Any(),
                                                                                             body=Any()).then(exception)

        result = self.repository.create(moodle_config, namespace="default")

        self.assertEqual(False, result)

    def test_create_raise_exception_when_unknown_exception_occurs(self):
        self.repository.custom_objects_api.create_namespaced_custom_object = Mock()
        exception = kubernetes.client.rest.ApiException(status=500)
        moodle_config = MoodleConfig(name="FakeName")
        when(self.repository.custom_objects_api.create_namespaced_custom_object).called_with(group=Any(),
                                                                                             version=Any(),
                                                                                             namespace=Any(),
                                                                                             plural=Any(),
                                                                                             body=Any()).then(exception)

        with self.assertRaises(Exception):
            self.repository.create(moodle_config, namespace="default")

    def test_delete_by_name_returns_false_when_object_doesnt_exists(self):
        self.repository.custom_objects_api.delete_namespaced_custom_object = Mock()
        exception = kubernetes.client.rest.ApiException(status=404)
        when(self.repository.custom_objects_api.delete_namespaced_custom_object).called_with(group=Any(),
                                                                                             version=Any(),
                                                                                             namespace=Any(),
                                                                                             plural=Any(),
                                                                                             name=Any()).then(exception)

        result = self.repository.delete_by_name("FakeName")

        self.assertEqual(False, result)

    def test_delete_by_name_raise_exception_when_unknown_exception_occurs(self):
        self.repository.custom_objects_api.delete_namespaced_custom_object = Mock()
        exception = kubernetes.client.rest.ApiException(status=500)
        when(self.repository.custom_objects_api.delete_namespaced_custom_object).called_with(group=Any(),
                                                                                             version=Any(),
                                                                                             namespace=Any(),
                                                                                             plural=Any(),
                                                                                             name=Any()).then(exception)

        with self.assertRaises(Exception):
            self.repository.delete_by_name("FakeName")


if __name__ == '__main__':
    unittest.main()
