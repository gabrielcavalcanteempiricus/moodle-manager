from backend.gateways import MoodleEventType
from backend.structs import MoodleConfig


def list_moodles_by_enteprise_use_case(kubernetes_repository,
                                       list_moodle_by_enterprise_gateway,
                                       enterprise):
    return list_moodle_by_enterprise_gateway(kubernetes_repository, enterprise)


def create_moodle_use_case(kubernetes_repository,
                           stan_repository,
                           create_moodle_gateway,
                           publish_create_moodle_event_gateway,
                           moodle_config) -> bool:
    result = create_moodle_gateway(kubernetes_repository, moodle_config)

    if result:
        next(publish_create_moodle_event_gateway(stan_repository, moodle_config))
        return True

    return False


def delete_moodle_use_case(kubernetes_repository,
                           stan_repository,
                           delete_moodle_gateway,
                           publish_moodle_event_gateway,
                           moodle_config: MoodleConfig) -> bool:
    result = delete_moodle_gateway(kubernetes_repository, moodle_config.name)

    if result:
        next(publish_moodle_event_gateway(stan_repository, MoodleEventType.DELETE, moodle_config))
        return True

    return False
