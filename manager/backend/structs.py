from dataclasses import dataclass, field
from uuid import uuid4


@dataclass
class MoodleConfig:
    name: str
    virtual_host: str = "http://localhost"
    version: str = "3.8"

    kind: str = "Moodle"
    apiVersion: str = "cloudspout.dev.br/v1"
    metadata: dict = field(default_factory=dict)
    spec: dict = field(default_factory=dict)

    def __post_init__(self):
        self.metadata = {"name": self.name}
        self.spec = {"name": self.name, "virtual_host": self.virtual_host, "version": self.version}
