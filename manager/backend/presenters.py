import json

from backend.structs import MoodleConfig


def present_moodle_config_list_as_json_serializable(moodle_config_list: [MoodleConfig]) -> [dict]:
    return json.dumps([{"name": moodle_config.name, "virtual_host": moodle_config.virtual_host, "version": moodle_config.version}
            for moodle_config in moodle_config_list])
