from abc import abstractmethod
from dataclasses import asdict
from uuid import uuid4

import kubernetes
from kubeconfig import KubeConfig

import settings
from backend.structs import MoodleConfig


class ConfigRepository:
    @abstractmethod
    def create(self, moodle_config, namespace) -> bool:
        raise NotImplementedError

    @abstractmethod
    def get_by_name(self, name: str, namespace) -> MoodleConfig:
        raise NotImplementedError

    def delete_by_name(self, name: str, namespace) -> bool:
        raise NotImplementedError


class KubernetesPersistenceRepository(ConfigRepository):
    def __init__(self):
        self.load_kubernetes_config()
        self.custom_objects_api = kubernetes.client.CustomObjectsApi()

    def load_kubernetes_config(self):
        conf = KubeConfig()
        conf.set_cluster(name='default', server=settings.K8S_SERVER_OVERRIDE.get())
        kubernetes.config.load_kube_config()

    def get_version(self):
        api = kubernetes.client.VersionApi()
        return api.get_code()

    def create(self, moodle_config, namespace) -> bool:
        try:
            self.custom_objects_api.create_namespaced_custom_object(
                group="cloudspout.dev.br",
                version="v1",
                namespace=namespace,
                plural="moodles",
                body=asdict(moodle_config)
            )
            return True
        except (kubernetes.client.rest.ApiException,) as e:
            if e.status == 409:
                return False
            raise

    def get_by_name(self, name: str, namespace="default") -> MoodleConfig:
        try:
            result = self.custom_objects_api.get_namespaced_custom_object(
                group="cloudspout.dev.br",
                version="v1",
                namespace=namespace,
                plural="moodles",
                name=name
            )
            return self._convert_k8s_result_to_moodleconfig(result)
        except (kubernetes.client.rest.ApiException,) as e:
            if e.status == 404:
                return False
            raise

    def get_by_enterprise(self, enterprise: str) -> [MoodleConfig]:
        try:
            result = self.custom_objects_api.list_namespaced_custom_object(
                group="cloudspout.dev.br",
                version="v1",
                namespace=enterprise,
                plural="moodles"
            )
            return [self._convert_k8s_result_to_moodleconfig(config) for config in result.get('items')]
        except (kubernetes.client.rest.ApiException) as e:
            if e.status == 404:
                return []

            raise

    def delete_by_name(self, name: str, namespace="default") -> bool:
        try:
            self.custom_objects_api.delete_namespaced_custom_object(
                group="cloudspout.dev.br",
                version="v1",
                namespace=namespace,
                plural="moodles",
                name=name
            )
            return True
        except (kubernetes.client.rest.ApiException,) as e:
            if e.status == 404:
                return False
            raise

    def _convert_k8s_result_to_moodleconfig(self, payload: dict) -> MoodleConfig:
        return MoodleConfig(
            name=payload['metadata'].get('name'),
            virtual_host=payload['spec'].get('virtual_host'),
            version=payload['spec'].get('version')
        )


class StanRepository:
    def __init__(self):
        self.nats = settings.NATS_CONNECTION.get()
        self.stan = settings.STAN_CONNECTION.get()

    async def connect(self, loop=False):
        await self.nats.connect(servers=settings.NATS_SERVERS.get(), loop=loop)
        await self.stan.connect(settings.NATS_CLUSTER_ID.get(), str(uuid4()), self.nats)

    async def disconnect(self, loop=False):
        await self.stan.close()
        await self.nats.close()

    async def publish_message(self, channel, payload):
        if not hasattr(payload, "encode"):
            raise TypeError("All NAT messages should be encodable as binary strings.")

        message = await self.stan.publish(channel, payload.encode())
        return message
