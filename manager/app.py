from aiohttp import web

from swagger_ui_bundle import swagger_ui_3_path


import asyncio
import connexion
import logging


logging.basicConfig(level=logging.INFO)
options = {'swagger_path': swagger_ui_3_path, 'strict_validation': True}


def create_app():
    app = connexion.AioHttpApp(__name__, specification_dir='specs/', options=options)
    app.add_api('moodleConfig.yaml')
    return app


async def start_httpserver():
    app = create_app()

    runner = web.AppRunner(app.app)
    await runner.setup()

    site = web.TCPSite(runner, '0.0.0.0', 8000)
    await site.start()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_httpserver())
    loop.run_forever()
