from functools import partial

from aiohttp import web

from backend.gateways import create_moodle, publish_create_moodle_event, list_moodles, delete_moodle, \
    publish_moodle_event
from backend.presenters import present_moodle_config_list_as_json_serializable
from backend.repositories import KubernetesPersistenceRepository, StanRepository
from backend.structs import MoodleConfig
from backend.usecases import create_moodle_use_case, list_moodles_by_enteprise_use_case, delete_moodle_use_case


def get_moodle_endpoint(enterprise):
    moodles = list_moodles_by_enteprise_use_case(kubernetes_repository=KubernetesPersistenceRepository(),
                                                 list_moodle_by_enterprise_gateway=list_moodles,
                                                 enterprise=enterprise)

    data = present_moodle_config_list_as_json_serializable(moodles)
    return web.Response(status=200, body=data, headers={'Content-Type': 'application/json'})


def create_moodle_endpoint(enterprise, body):
    moodle_config = MoodleConfig(**body)
    create_moodle_gateway = partial(create_moodle, enterprise=enterprise)

    created = create_moodle_use_case(kubernetes_repository=KubernetesPersistenceRepository(),
                                     stan_repository=StanRepository(),
                                     create_moodle_gateway=create_moodle_gateway,
                                     publish_create_moodle_event_gateway=publish_create_moodle_event,
                                     moodle_config=moodle_config)

    if not created:
        return web.Response(status=409, headers={'Content-Type': 'application/json'})

    return web.Response(status=201, headers={'Content-Type': 'application/json'})


def delete_moodle_endpoint(enterprise, moodle_instance):
    moodle_config = MoodleConfig(name=moodle_instance)
    delete_moodle_gateway = partial(delete_moodle, enterprise=enterprise)

    deleted = delete_moodle_use_case(kubernetes_repository=KubernetesPersistenceRepository(),
                                     stan_repository=StanRepository(),
                                     delete_moodle_gateway=delete_moodle_gateway,
                                     publish_moodle_event_gateway=publish_moodle_event,
                                     moodle_config=moodle_config)

    if not deleted:
        return web.Response(status=404, headers={'Content-Type': 'application/json'})

    return web.Response(status=204, headers={'Content-Type': 'application/json'})
