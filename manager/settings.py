import os
from contextvars import ContextVar

from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN

DEBUG = ContextVar('DEBUG', default=os.getenv('DEBUG', True))
NATS_SERVERS = ContextVar('NATS_SERVERS', default=[os.getenv('NATS_SERVER')])
NATS_CLUSTER_ID = ContextVar('NATS_CLUSTER_ID', default=os.getenv('NATS_CLUSTER_ID'))
K8S_SERVER_OVERRIDE = ContextVar('K8S_SERVER_OVERRIDE', default=os.getenv('K8S_SERVER_OVERRIDE', '127.0.0.1'))

NATS_CONNECTION = ContextVar('nats', default=NATS())
STAN_CONNECTION = ContextVar('stan', default=STAN())
